/*
 *  segmentation
 *
 *  license: GPL2
 *  Copyright (C) i@liufang.org.cn
 *  Author: fang.liu
 */

#include "str.h"

//创建一个seg字符串
seg_str* create_seg_str(const char* str)
{
	seg_str* p_seg_str = (seg_str*)malloc(sizeof(seg_str));
	char* src_str;
	int len = strlen(str);
	p_seg_str->str = (char*) malloc(len+1);
	strcpy(p_seg_str->str, str);
	p_seg_str->len = len;
	return p_seg_str;
}

//链接字符串
seg_str* cat_str(seg_str *des, seg_str * src)
{
	char *str;
	int len;
	if(des == NULL) {
		des = create_seg_str(src->str);
	} else {
		//重新分配内存, 包含1个空格一个\0
		realloc(des->str, src->len + des->len + 2);
		str = des->str;
		str += des->len;
		*str = ' ';
		str++;
		strncpy(str, src->str, src->len);
		str += src->len;
		*str = '\0';
		des->len += src->len + 1;
	}
	return des;
}

